CREATE TABLE `author`
(
    `author_id`  bigint(20) NOT NULL AUTO_INCREMENT,
    `first_name` varchar(255) DEFAULT NULL,
    `last_name`  varchar(255) DEFAULT NULL,
    `birth_year` int(11)      DEFAULT NULL,
    `death_year` int(11)      DEFAULT NULL,
    PRIMARY KEY (`author_id`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 52
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_0900_ai_ci