CREATE TABLE `location`
(
    `location_id` bigint(20) NOT NULL AUTO_INCREMENT,
    `continent`   enum ('EUROPE','AFRICA','ASIA','AUSTRALIA_OCEANIA','NORTH_AMERICA','SOUTH_AMERICA','ANTARCTICA') DEFAULT NULL,
    `country`     varchar(255)                                                                                     DEFAULT NULL,
    `city`        varchar(255)                                                                                     DEFAULT NULL,
    PRIMARY KEY (`location_id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_0900_ai_ci