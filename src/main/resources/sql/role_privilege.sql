CREATE TABLE `role_privilege`
(
    `role_privilege_id` bigint(20) NOT NULL AUTO_INCREMENT,
    `role_id`           bigint(20) DEFAULT NULL,
    `privilege_id`      bigint(20) DEFAULT NULL,
    PRIMARY KEY (`role_privilege_id`),
    KEY `role_privilege_role_fk_idx` (`role_id`),
    KEY `role_privilege_privilege_fk_idx` (`privilege_id`),
    CONSTRAINT `role_privilege_privilege_fk` FOREIGN KEY (`privilege_id`) REFERENCES `privilege` (`privilege_id`) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT `role_privilege_role_fk` FOREIGN KEY (`role_id`) REFERENCES `role` (`role_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB
  AUTO_INCREMENT = 15
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_0900_ai_ci