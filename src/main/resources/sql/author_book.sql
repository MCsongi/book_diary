CREATE TABLE `author_book`
(
    `author_book_id` bigint(20) NOT NULL AUTO_INCREMENT,
    `author_id`      bigint(20) DEFAULT NULL,
    `book_id`        bigint(20) DEFAULT NULL,
    PRIMARY KEY (`author_book_id`),
    KEY `author_book_author_fk_idx` (`author_id`),
    KEY `author_book_book_fk_idx` (`book_id`),
    CONSTRAINT `author_book_author_fk` FOREIGN KEY (`author_id`) REFERENCES `author` (`author_id`),
    CONSTRAINT `author_book_book_fk` FOREIGN KEY (`book_id`) REFERENCES `book` (`book_id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_0900_ai_ci