CREATE TABLE `user_book`
(
    `user_book_id` bigint(20) NOT NULL AUTO_INCREMENT,
    `user_id`      bigint(20) DEFAULT NULL,
    `book_id`      bigint(20) DEFAULT NULL,
    PRIMARY KEY (`user_book_id`),
    KEY `user_book_book_fk_idx` (`book_id`),
    KEY `user_book_user_fk_idx` (`user_id`),
    CONSTRAINT `user_book_book_fk` FOREIGN KEY (`book_id`) REFERENCES `book` (`book_id`),
    CONSTRAINT `user_book_user_fk` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_0900_ai_ci