CREATE TABLE `author_details`
(
    `author_details_id` bigint(20) NOT NULL AUTO_INCREMENT,
    `about`             longtext,
    `author_id`         bigint(20) DEFAULT NULL,
    `user_id`           bigint(20) DEFAULT NULL,
    PRIMARY KEY (`author_details_id`),
    KEY `author_details_author_fk_idx` (`author_id`),
    KEY `author_details_user_fk_idx` (`user_id`),
    CONSTRAINT `author_details_author_fk` FOREIGN KEY (`author_id`) REFERENCES `author` (`author_id`),
    CONSTRAINT `author_details_user_fk` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_0900_ai_ci