CREATE TABLE `privilege`
(
    `privilege_id`   bigint(20) NOT NULL AUTO_INCREMENT,
    `privilege_name` varchar(255) DEFAULT NULL,
    PRIMARY KEY (`privilege_id`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 5
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_0900_ai_ci