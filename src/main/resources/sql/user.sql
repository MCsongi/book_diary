CREATE TABLE `user`
(
    `user_id`       bigint(20) NOT NULL AUTO_INCREMENT,
    `first_name`    varchar(255) DEFAULT NULL,
    `last_name`     varchar(255) DEFAULT NULL,
    `username`      varchar(255) DEFAULT NULL,
    `password`      varchar(255) DEFAULT NULL,
    `email`         varchar(255) DEFAULT NULL,
    `birth_date`    date         DEFAULT NULL,
    `enabled`       tinyint(2)   DEFAULT NULL,
    `token_expired` tinyint(2)   DEFAULT NULL,
    PRIMARY KEY (`user_id`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 42
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_0900_ai_ci