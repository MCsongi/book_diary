CREATE TABLE `book_location`
(
    `book_location_id` bigint(20) NOT NULL AUTO_INCREMENT,
    `book_id`          bigint(20) DEFAULT NULL,
    `location_id`      bigint(20) DEFAULT NULL,
    PRIMARY KEY (`book_location_id`),
    KEY `book_location_book_fk_idx` (`book_id`),
    KEY `book_location_location_fk_idx` (`location_id`),
    CONSTRAINT `book_location_book_fk` FOREIGN KEY (`book_id`) REFERENCES `book` (`book_id`),
    CONSTRAINT `book_location_location_fk` FOREIGN KEY (`location_id`) REFERENCES `location` (`location_id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_0900_ai_ci