CREATE TABLE `book_details`
(
    `book_details_id` bigint(20) NOT NULL AUTO_INCREMENT,
    `plot`            longtext,
    `characters`      longtext,
    `number_of_pages` int(11)    DEFAULT NULL,
    `book_id`         bigint(20) DEFAULT NULL,
    `user_id`         bigint(20) DEFAULT NULL,
    PRIMARY KEY (`book_details_id`),
    KEY `book_details_book_fk_idx` (`book_id`),
    KEY `book_details_user_fk_idx` (`user_id`),
    CONSTRAINT `book_details_book_fk` FOREIGN KEY (`book_id`) REFERENCES `book` (`book_id`),
    CONSTRAINT `book_details_user_fk` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_0900_ai_ci