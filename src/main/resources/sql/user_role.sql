CREATE TABLE `user_role`
(
    `user_role_id` bigint(20) NOT NULL AUTO_INCREMENT,
    `user_id`      bigint(20) DEFAULT NULL,
    `role_id`      bigint(20) DEFAULT NULL,
    PRIMARY KEY (`user_role_id`),
    KEY `user_role_role_idx` (`role_id`),
    KEY `user_role_user_fk_idx` (`user_id`),
    CONSTRAINT `user_role_role_fk` FOREIGN KEY (`role_id`) REFERENCES `role` (`role_id`) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT `user_role_user_fk` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB
  AUTO_INCREMENT = 13
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_0900_ai_ci