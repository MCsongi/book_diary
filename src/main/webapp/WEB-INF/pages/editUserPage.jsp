<%@ taglib prefix="th" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>

<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Users</title>

    <link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/common.css" rel="stylesheet">
</head>
<body>
<div class="container">
    <%--@elvariable id="userForm" type="com.sda.cluj4.finalproject.security.model"--%>
    <form:form modelAttribute="userForm" action="${contextPath}/admin/user/edit/{id}(id=${userForm.user_id})"
               class="form-signin" method="POST">
        <h2 class="form-signin-heading">Edit user</h2>

        <spring:bind path="user_id">
            <div class="form-group ${status.error ? 'has-error' : ''}">
                <form:input type="hidden" path="user_id" value="${userForm.user_id}"></form:input>
                <form:errors path="user_id"></form:errors>
            </div>
        </spring:bind>

        <spring:bind path="username">
            <div class="form-group ${status.error ? 'has-error' : ''}">
                Username: <form:input type="text" readonly="true" path="username" class="form-control" placeholder="Username"></form:input>
                <form:errors path="username"></form:errors>
            </div>
        </spring:bind>

        <spring:bind path="firstName">
            <div class="form-group ${status.error ? 'has-error' : ''}">
                First Name: <form:input type="text" path="firstName" class="form-control" placeholder="First Name"
                            autofocus="true"></form:input>
                <form:errors path="firstName"></form:errors>
            </div>
        </spring:bind>

        <spring:bind path="lastName">
            <div class="form-group ${status.error ? 'has-error' : ''}">
                Last Name: <form:input type="text" path="lastName" class="form-control" placeholder="Last Name"></form:input>
                <form:errors path="lastName"></form:errors>
            </div>
        </spring:bind>

        <spring:bind path="password">
            <div class="form-group ${status.error ? 'has-error' : ''}">
                <form:input type="hidden" path="password" class="form-control" placeholder="Password"></form:input>
                <form:errors path="password"></form:errors>
            </div>
        </spring:bind>

        <spring:bind path="email">
            <div class="form-group ${status.error ? 'has-error' : ''}">
                E-mail: <form:input type="text" path="email" class="form-control" placeholder="Email"></form:input>
                <form:errors path="email"></form:errors>
            </div>
        </spring:bind>

        <spring:bind path="birthDate">
            <div class="form-group ${status.error ? 'has-error' : ''}">
                Date of birth: <form:input type="text" path="birthDate" class="form-control" placeholder="Birth Date: yyyy-MM-dd"></form:input>
                <form:errors path="birthDate"></form:errors>
            </div>
        </spring:bind>

        <spring:bind path="enabled">
            <div class="form-group ${status.error ? 'has-error' : ''}">
                <form:input type="hidden" path="enabled" class="form-control" placeholder="Enabled"></form:input>
                <form:errors path="enabled"></form:errors>
            </div>
        </spring:bind>

        <spring:bind path="tokenExpired">
            <div class="form-group ${status.error ? 'has-error' : ''}">
                <form:input type="hidden" path="tokenExpired" class="form-control"
                            placeholder="Token Expired"></form:input>
                <form:errors path="tokenExpired"></form:errors>
            </div>
        </spring:bind>

        <button class="btn btn-lg btn-primary btn-block" type="submit" value="Save">Save edits</button>
        <button style="background-color: #dddddd"><a href="/admin/user/list">Cancel</a></button>
    </form:form>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="${contextPath}/resources/js/bootstrap.min.js"></script>
</body>
</html>