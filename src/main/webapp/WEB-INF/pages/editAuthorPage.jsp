<%@ taglib prefix="th" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<!DOCTYPE html>

<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Authors</title>

    <link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/common.css" rel="stylesheet">
</head>
<body>
<div class="container">
    <%--@elvariable id="authorForm" type="com.sda.cluj4.finalproject.model"--%>
    <form:form modelAttribute="authorForm" action="${contextPath}/admin/author/edit/{id}(id=${authorForm.author_id})"
               class="form-signin" method="POST" >
        <h2 class="form-signin-heading">Edit author</h2>

        <spring:bind path="author_id">
            <div class="form-group ${status.error ? 'has-error' : ''}">
                <form:input type="hidden" path="author_id" value="${authorForm.author_id}"></form:input>
                <form:errors path="author_id"></form:errors>
            </div>
        </spring:bind>

        <spring:bind path="firstName">
            <div class="form-group ${status.error ? 'has-error' : ''}">
                First Name: <form:input type="text" path="firstName" class="form-control" placeholder="First Name" autofocus = "true"></form:input>
                <form:errors path="firstName"></form:errors>
            </div>
        </spring:bind>

        <spring:bind path="lastName">
            <div class="form-group ${status.error ? 'has-error' : ''}">
                Last Name: <form:input type="text" path="lastName" class="form-control" placeholder="Last Name"></form:input>
                <form:errors path="lastName"></form:errors>
            </div>
        </spring:bind>

        <spring:bind path="birthYear">
            <div class="form-group ${status.error ? 'has-error' : ''}">
                Year of birth: <form:input type="text" path="birthYear" class="form-control" placeholder="yyyy"></form:input>
                <form:errors path="birthYear"></form:errors>
            </div>
        </spring:bind>

        <spring:bind path="deathYear">
            <div class="form-group ${status.error ? 'has-error' : ''}">
                Year of death: <form:input type="text" path="deathYear" class="form-control" placeholder="yyyy"></form:input>
                <form:errors path="deathYear"></form:errors>
            </div>
        </spring:bind>

        <button class="btn btn-lg btn-primary btn-block" type="submit" value="Save">Save edits</button>
        <button style="background-color: #dddddd"><a href="/admin/author/list">Cancel</a></button>

    </form:form>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="${contextPath}/resources/js/bootstrap.min.js"></script>
</body>
</html>