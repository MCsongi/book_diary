<%@ taglib prefix="th" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>

<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Roles</title>

    <link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/common.css" rel="stylesheet">
</head>

<body>
<div class="container">

    <%--@elvariable id="userForm" type="com.sda.cluj4.finalproject.security.model"--%>
    <form:form modelAttribute="userForm" action="${contextPath}/admin/user/roles/{id}(id=${userForm.user_id})"
               id="roleForm" method="POST" class="form-signin">
        <h2 class="form-signin-heading">Set new Roles</h2>

        <div class="form-group">
            <form:input type="hidden" path="user_id" value="${userForm.user_id}"></form:input>
        </div>

        <h4>Please choose one or many roles:</h4>

        <div class="form-group">
            <select class="form-control" name="roles" form="roleForm" size="2" multiple>
                <c:forEach items="${roleList}" var="role">
                    <option value="${role.role_id}">${role.name}</option>
                </c:forEach>
            </select>
        </div>

        <div class="form-group">
            <input type="hidden" class="form-control">
            <input type="submit">
            <button style="background-color: #dddddd"><a href="/admin/user/list">Cancel</a></button>
        </div>
    </form:form>

</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="${contextPath}/resources/js/bootstrap.min.js"></script>

</body>
</html>