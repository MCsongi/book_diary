<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Books</title>
    <link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/common.css" rel="stylesheet">
</head>

<body>

<div class="container">
    <div align="left">
        <button><a href="/admin">Home</a></button>
        <button><a href="/admin/author/list">Manage Authors</a></button>
    </div>
    <div align="right">
        <c:if test="${pageContext.request.userPrincipal.name != null}">
            <form id="logoutForm" method="POST" action="${contextPath}/logout">
                <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
            </form>

            <h5>Logged in with role Admin: <span>${pageContext.request.userPrincipal.name}</span>
                <button><a onclick="document.forms['logoutForm'].submit()">Logout</a></button>
            </h5>
        </c:if>
    </div>
</div>

<div class="container">
    <h2>Books List</h2>

    <div class="table-responsive">
        <table class="table table-bordered css-serial">
            <tr>
                <th>#</th>
                <th>Author</th>
                <th>Title</th>
                <th>Release Year</th>
                <th>Genre</th>
                <th>Plot happens in year</th>
            </tr>
            <c:forEach items="${bookList}" var="book">
                <tr class="panel">

                    <td></td>
                    <td>${book.authors}</td>
                    <td><strong>${book.title}</strong> (${book.originalTitle})</td>
                    <td>${book.releaseYear}</td>
                    <td>${book.genre}</td>
                    <td><i>${book.plotPeriod}</i></td>
                    <td>
                        <form:form method="GET" action="${contextPath}/admin/book/edit/{id}">
                            <input type="hidden" name="book_id"
                                   value="${book.book_id}">
                            <input type="submit" value="Edit">
                        </form:form>
                    </td>

                    <td>
                        <form:form method="GET" action="${contextPath}/admin/book/genre/{id}">
                            <input type="hidden" name="book_id"
                                   value="${book.book_id}">
                            <input type="submit" value="Set Genre">
                        </form:form>
                    </td>

                    <td>
                        <form:form method="GET" action="${contextPath}/admin/book/author/{id}">
                            <input type="hidden" name="book_id"
                                   value="${book.book_id}">
                            <input type="submit" value="Set Authors">
                        </form:form>
                    </td>

                    <td>
                        <form:form method="POST" action="${contextPath}/admin/book/delete/{id}">
                            <input type="hidden" name="book_id"
                                   value="${book.book_id}">
                            <input type="submit" value="Delete">
                        </form:form>
                    </td>
                </tr>
            </c:forEach>
        </table>
    </div>

    <button><a href="/admin/book/add">Add Book</a></button>

    <form:form method="DELETE" action="${contextPath}/admin/book/list/delete">
        <input type="submit" value="Delete all Books">
    </form:form>

</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="${contextPath}/resources/js/bootstrap.min.js"></script>

</body>
</html>