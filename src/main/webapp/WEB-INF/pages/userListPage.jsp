<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Users</title>
    <link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/common.css" rel="stylesheet">
</head>

<body>

<div class="container">
    <div align="left">
        <button><a href="/admin">Home</a></button>
        <button><a href="/admin/role_privileges/list">Manage Roles and Privileges</a></button>
    </div>
    <div align="right">
        <c:if test="${pageContext.request.userPrincipal.name != null}">
            <form id="logoutForm" method="POST" action="${contextPath}/logout">
                <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
            </form>

            <h5>Logged in with role Admin: <span>${pageContext.request.userPrincipal.name}</span>
                <button><a onclick="document.forms['logoutForm'].submit()">Logout</a></button>
            </h5>
        </c:if>
    </div>
</div>

<div class="container">
    <h2>Users List</h2>

    <div class="table-responsive">
        <table class="table table-bordered css-serial">
            <tr>
                <th>#</th>
                <th>User Id</th>
                <th>Username</th>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Date of Birth</th>
                <th>E-mail</th>
                <th>Roles</th>
                <th></th>
                <th></th>
                <th></th>
            </tr>
            <c:forEach items="${userList}" var="user">
                <tr class="panel">

                    <td></td>
                    <td>${user.user_id}</td>
                    <td>${user.username}</td>
                    <td>${user.firstName}</td>
                    <td>${user.lastName}</td>
                    <td>${user.birthDate}</td>
                    <td>${user.email}</td>
                    <td>${user.roles}</td>
                    <td>
                        <form:form method="GET" action="${contextPath}/admin/user/edit/{id}">
                            <input type="hidden" name="user_id"
                                   value="${user.user_id}">
                            <input type="submit" value="Edit">
                        </form:form>
                    </td>
                    <td>
                        <form:form method="GET" action="${contextPath}/admin/user/roles/{id}">
                            <input type="hidden" name="user_id"
                                   value="${user.user_id}">
                            <input type="submit" value="Roles">
                        </form:form>
                    </td>
                    <td>
                        <form:form method="POST" action="${contextPath}/admin/user/delete/{id}">
                            <input type="hidden" name="user_id"
                                   value="${user.user_id}">
                            <input type="submit" value="Delete">
                        </form:form>
                    </td>
                </tr>
            </c:forEach>
        </table>
    </div>

    <form:form method="DELETE" action="${contextPath}/admin/user/list/delete">
        <input type="submit" value="Delete all Users">
    </form:form>

</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="${contextPath}/resources/js/bootstrap.min.js"></script>

</body>
</html>