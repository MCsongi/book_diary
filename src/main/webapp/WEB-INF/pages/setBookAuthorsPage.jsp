<%@ taglib prefix="th" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>

<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Authors</title>

    <link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/common.css" rel="stylesheet">
</head>

<body>
<div class="container">

    <%--@elvariable id="bookForm" type="com.sda.cluj4.finalproject.model"--%>
    <form:form modelAttribute="bookForm" action="${contextPath}/admin/book/author/{id}(id=${bookForm.book_id})"
               id="authorForm" method="POST" class="form-signin">
        <h2 class="form-signin-heading">Set Author</h2>

        <div class="form-group">
            <form:input type="hidden" path="book_id" value="${bookForm.book_id}"></form:input>
        </div>

        <h4>Please choose one or many authors:</h4>

        <div class="form-group">
            <select class="form-control" name="authors" form="authorForm" size="4" multiple>
                <c:forEach items="${authorList}" var="author">
                    <option value="${author.author_id}">${author}</option>
                </c:forEach>
            </select>
        </div>

        <div class="form-group">
            <input type="hidden" class="form-control">
            <input type="submit">
            <button style="background-color: #dddddd"><a href="/admin/book/list">Cancel</a></button>
            <button style="background-color: #dddddd"><a href="/admin/author/add">Add New Author</a></button>
        </div>
    </form:form>

</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="${contextPath}/resources/js/bootstrap.min.js"></script>

</body>
</html>