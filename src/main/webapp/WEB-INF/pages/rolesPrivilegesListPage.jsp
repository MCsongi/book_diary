<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Roles and Privileges</title>
    <link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/common.css" rel="stylesheet">
</head>

<body>

<div class="container">
    <div align="left">
        <button><a href="/admin">Home</a></button>
        <button><a href="/admin/user/list">Manage Users</a></button>
    </div>
    <div align="right">
        <c:if test="${pageContext.request.userPrincipal.name != null}">
            <form id="logoutForm" method="POST" action="${contextPath}/logout">
                <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
            </form>

            <h5>Logged in with role Admin: <span>${pageContext.request.userPrincipal.name}</span>
                <button><a onclick="document.forms['logoutForm'].submit()">Logout</a></button>
            </h5>
        </c:if>
    </div>
</div>

<div class="container">
    <h2>Roles List</h2>

    <div class="table-responsive">
        <table class="table table-bordered css-serial">
            <tr>
                <th>#</th>
                <th>Role Id</th>
                <th>Role Name</th>
                <th>Privileges</th>
                <th></th>
                <th></th>
            </tr>
            <c:forEach items="${roleList}" var="role">
                <tr class="panel">

                    <td></td>
                    <td>${role.role_id}</td>
                    <td>${role.name}</td>
                    <td>${role.privileges}</td>
                    <td>
                        <form:form method="GET" action="${contextPath}/admin/role/privileges/{id}">
                            <input type="hidden" name="role_id"
                                   value="${role.role_id}">
                            <input type="submit" value="Privileges">
                        </form:form>
                    </td>
                    <td>
                        <form:form method="POST" action="${contextPath}/admin/role/delete/{id}">
                            <input type="hidden" name="role_id"
                                   value="${role.role_id}">
                            <input type="submit" value="Delete">
                        </form:form>
                    </td>
                </tr>
            </c:forEach>
        </table>
    </div>

    <button><a href="/admin/role/add">Add Role</a></button>

    <form:form method="DELETE" action="${contextPath}/admin/role/list/delete">
        <input type="submit" value="Delete all Roles">
    </form:form>

</div>

<div class="container">
    <h2>Privileges List</h2>

    <div class="table-responsive">
        <table class="table table-bordered css-serial">
            <tr>
                <th>#</th>
                <th>Privilege Id</th>
                <th>Privilege Name</th>
                <th>Privilege Roles</th>
                <th></th>
            </tr>
            <c:forEach items="${privilegeList}" var="privilege">
                <tr class="panel">

                    <td></td>
                    <td>${privilege.privilege_id}</td>
                    <td>${privilege.name}</td>
                    <td>${privilege.roles}</td>
                    <td>
                        <form:form method="POST" action="${contextPath}/admin/privilege/delete/{id}">
                            <input type="hidden" name="privilege_id"
                                   value="${privilege.privilege_id}">
                            <input type="submit" value="Delete">
                        </form:form>
                    </td>
                </tr>
            </c:forEach>
        </table>
    </div>

    <button><a href="/admin/privilege/add">Add Privilege</a></button>

    <form:form method="DELETE" action="${contextPath}/admin/privilege/list/delete">
        <input type="submit" value="Delete all Privileges">
    </form:form>

</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="${contextPath}/resources/js/bootstrap.min.js"></script>

</body>
</html>