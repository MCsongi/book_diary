<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Authors</title>
    <link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/common.css" rel="stylesheet">
</head>

<body>

<div class="container">
    <div align="left">
        <button><a href="/admin">Home</a></button>
        <button><a href="/admin/book/list">Manage Books</a></button>
    </div>
    <div align="right">
        <c:if test="${pageContext.request.userPrincipal.name != null}">
            <form id="logoutForm" method="POST" action="${contextPath}/logout">
                <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
            </form>

            <h5>Logged in with role Admin: <span>${pageContext.request.userPrincipal.name}</span>
                <button><a onclick="document.forms['logoutForm'].submit()">Logout</a></button>
            </h5>
        </c:if>
    </div>
</div>

<div class="container">
    <h2>Authors List</h2>

    <div class="table-responsive">
        <table class="table table-bordered css-serial">
            <tr>
                <th>#</th>
                <th>Name</th>
                <th>Year of Birth</th>
                <th>Year of Death</th>
                <th></th>
                <th></th>
            </tr>
            <c:forEach items="${authorList}" var="author">
                <tr class="panel">

                    <td></td>
                    <td hidden>${author.author_id}</td>
                    <td>${author.fullName}</td>
                    <td>${author.birthYear}</td>
                    <td>${author.deathYear}</td>
                    <td>
                        <form:form method="GET" action="${contextPath}/admin/author/edit/{id}">
                            <input type="hidden" name="author_id"
                                   value="${author.author_id}">
                            <input type="submit" value="Edit">
                        </form:form>
                    </td>
                    <td>
                        <form:form method="POST" action="${contextPath}/admin/author/delete/{id}">
                            <input type="hidden" name="author_id"
                                   value="${author.author_id}">
                            <input type="submit" value="Delete">
                        </form:form>
                    </td>
                </tr>
            </c:forEach>
        </table>
    </div>

    <button><a href="/admin/author/add">Add Author</a></button>

    <form:form method="DELETE" action="${contextPath}/admin/author/list/delete">
        <input type="submit" value="Delete all Authors">
    </form:form>

</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="${contextPath}/resources/js/bootstrap.min.js"></script>

</body>
</html>