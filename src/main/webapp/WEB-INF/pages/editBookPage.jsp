<%@ taglib prefix="th" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<!DOCTYPE html>

<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Books</title>

    <link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/common.css" rel="stylesheet">
</head>
<body>
<div class="container">
    <%--@elvariable id="bookForm" type="com.sda.cluj4.finalproject.model"--%>
    <form:form modelAttribute="bookForm" action="${contextPath}/admin/book/edit/{id}(id=${bookForm.book_id})"
               class="form-signin" method="POST" >
        <h2 class="form-signin-heading">Edit book</h2>

        <spring:bind path="book_id">
            <div class="form-group ${status.error ? 'has-error' : ''}">
                <form:input type="hidden" path="book_id" value="${bookForm.book_id}"></form:input>
                <form:errors path="book_id"></form:errors>
            </div>
        </spring:bind>

        <spring:bind path="title">
            <div class="form-group ${status.error ? 'has-error' : ''}">
                Title: <form:input type="text" path="title" class="form-control" placeholder="Title" autofocus = "true"></form:input>
                <form:errors path="title"></form:errors>
            </div>
        </spring:bind>

        <spring:bind path="originalTitle">
            <div class="form-group ${status.error ? 'has-error' : ''}">
                Original Title: <form:input type="text" path="originalTitle" class="form-control" placeholder="Original Title"></form:input>
                <form:errors path="originalTitle"></form:errors>
            </div>
        </spring:bind>

        <spring:bind path="releaseYear">
            <div class="form-group ${status.error ? 'has-error' : ''}">
                Release Year: <form:input type="text" path="releaseYear" class="form-control" placeholder="yyyy"></form:input>
                <form:errors path="releaseYear"></form:errors>
            </div>
        </spring:bind>

        <spring:bind path="startYear">
            <div class="form-group ${status.error ? 'has-error' : ''}">
                Plot begins in year: <form:input type="text" path="startYear" class="form-control" placeholder="yyyy"></form:input>
                <form:errors path="startYear"></form:errors>
            </div>
        </spring:bind>

        <spring:bind path="endYear">
            <div class="form-group ${status.error ? 'has-error' : ''}">
                Plot ends in year: <form:input type="text" path="endYear" class="form-control" placeholder="yyyy"></form:input>
                <form:errors path="endYear"></form:errors>
            </div>
        </spring:bind>

        <button class="btn btn-lg btn-primary btn-block" type="submit" value="Save">Save edits</button>
        <button style="background-color: #dddddd"><a href="/admin/book/list">Cancel</a></button>

    </form:form>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="${contextPath}/resources/js/bootstrap.min.js"></script>
</body>
</html>