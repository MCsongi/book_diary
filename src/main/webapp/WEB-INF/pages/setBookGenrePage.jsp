<%@ taglib prefix="th" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>

<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Genre</title>

    <link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/common.css" rel="stylesheet">
</head>

<body>
<div class="container">

    <%--@elvariable id="bookForm" type="com.sda.cluj4.finalproject.model"--%>
    <form:form modelAttribute="bookForm" action="${contextPath}/admin/book/genre/{id}(id=${bookForm.book_id})"
               id="genreForm" method="POST" class="form-signin">
        <h2 class="form-signin-heading">Set Book Genre</h2>

        <div class="form-group">
            <form:input type="hidden" path="book_id" value="${bookForm.book_id}"></form:input>
        </div>

        <h4>Please choose one:</h4>

        <div class="form-group">
            <select class="form-control" name="genre" form="genreForm" size="4">
                <c:forEach var="genre" items="${genreList}">
                    <option value="${genre.getName()}">${genre}</option>
                </c:forEach>
            </select>
        </div>

        <div class="form-group">
            <input type="hidden" class="form-control">
            <input type="submit">
            <button style="background-color: #dddddd"><a href="/admin/book/list">Cancel</a></button>
        </div>
    </form:form>

</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="${contextPath}/resources/js/bootstrap.min.js"></script>

</body>
</html>