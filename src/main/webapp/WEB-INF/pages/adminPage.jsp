<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Admin Dashboard</title>
    <link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/common.css" rel="stylesheet">
</head>

<body>
<div class="container">
    <c:if test="${pageContext.request.userPrincipal.name != null}">
        <form id="logoutForm" method="POST" action="${contextPath}/logout">
            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
        </form>

        <h4>Logged in with role Admin: ${pageContext.request.userPrincipal.name} | <a
                onclick="document.forms['logoutForm'].submit()">Logout</a>
        </h4>

        <div class="container">
            <h2>App Management</h2>

            <div class="table-responsive">
                <table class="table table-bordered">
                    <tr>
                        <th>Objects</th>
                        <th>Tools</th>
                    </tr>
                    <tr class="panel">
                        <td>Users</td>
                        <td>
                            <button><a href="/admin/user/list">Manage Users</a></button>
                        </td>
                    </tr>
                    <tr class="panel">
                        <td>Roles and Privileges</td>
                        <td>
                            <button><a href="/admin/role_privileges/list">Manage Roles and Privileges</a></button>
                        </td>
                    </tr>
                    <tr class="panel">
                        <td>Authors</td>
                        <td>
                            <button><a href="/admin/author/list">Manage Authors</a></button>
                        </td>
                    </tr>
                    <tr class="panel">
                        <td>Books</td>
                        <td>
                            <button><a href="/admin/book/list">Manage Books</a></button>
                        </td>
                    </tr>
                    <tr class="panel">
                        <td>My Account</td>
                        <td>
                            <button><a href="/welcome">Manage my readings</a></button>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </c:if>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="${contextPath}/resources/js/bootstrap.min.js"></script>
</body>
</html>
