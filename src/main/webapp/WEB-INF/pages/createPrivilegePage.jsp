<%@ taglib prefix="th" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<!DOCTYPE html>

<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Privileges</title>

    <link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/common.css" rel="stylesheet">
</head>
<body>
<div class="container">
    <%--@elvariable id="privilegeForm" type="com.sda.cluj4.finalproject.model"--%>
    <form:form method="POST" action="${contextPath}/admin/privilege/add" modelAttribute="privilegeForm" class="form-signin">
        <h2 class="form-signin-heading">Add new Privilege</h2>

        <spring:bind path="name">
            <div class="form-group ${status.error ? 'has-error' : ''}">
                Privilege Name: <form:input type="text" path="name" class="form-control" placeholder="Name" autofocus = "true"></form:input>
                <form:errors path="name"></form:errors>
            </div>
        </spring:bind>

        <button class="btn btn-lg btn-primary btn-block" type="submit" value="Save">Add Privilege</button>
        <button style="background-color: #dddddd"><a href="/admin/role_privileges/list">Cancel</a></button>

    </form:form>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="${contextPath}/resources/js/bootstrap.min.js"></script>
</body>
</html>