<%@ taglib prefix="th" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>

<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Privileges</title>

    <link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/common.css" rel="stylesheet">
</head>

<body>
<div class="container">

    <%--@elvariable id="roleForm" type="com.sda.cluj4.finalproject.security.model"--%>
    <form:form modelAttribute="roleForm" action="${contextPath}/admin/role/privileges/{id}(id=${roleForm.role_id})"
               id="privilegeForm" method="POST" class="form-signin">
        <h2 class="form-signin-heading">Set new Privileges</h2>

        <div class="form-group">
            <form:input type="hidden" path="role_id" value="${roleForm.role_id}"></form:input>
        </div>

        <h4>Please choose one or many privileges:</h4>

        <div class="form-group">
            <select class="form-control" name="privileges" form="privilegeForm" size="2" multiple>
                <c:forEach items="${privilegeList}" var="privilege">
                    <option value="${privilege.privilege_id}">${privilege.name}</option>
                </c:forEach>
            </select>
        </div>

        <div class="form-group">
            <input type="hidden" class="form-control">
            <input type="submit">
            <button style="background-color: #dddddd"><a href="/admin/role_privileges/list">Cancel</a></button>
        </div>

    </form:form>

</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="${contextPath}/resources/js/bootstrap.min.js"></script>

</body>
</html>