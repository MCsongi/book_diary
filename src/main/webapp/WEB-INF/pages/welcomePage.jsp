<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Welcome</title>
    <link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/common.css" rel="stylesheet">
</head>

<body>

<div class="container">
    <c:if test="${pageContext.request.userPrincipal.name != null}">
        <form id="logoutForm" method="POST" action="${contextPath}/logout">
            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
        </form>

        <h4>Welcome   ${pageContext.request.userPrincipal.name}   | <a onclick="document.forms['logoutForm'].submit()">Logout</a>
        </h4>
    </c:if>
</div>

<div class="container">

    <h2 class="form-signin-heading">My Book Diary</h2>

    <div>
        <table>
            <tr>
                <td rowspan="5">
                    <img src="${contextPath}/resources/images/user_page_image.jfif" style="margin: 20px">
                </td>
                <td colspan="2">
                    <h4><strong>My Readings in Numbers</strong></h4>
                </td>
            </tr>
            <tr>
                <td><strong>Number of read authors: </strong></td>
                <td></td>
            </tr>
            <tr>
                <td><strong>Number of read books: </strong></td>
                <td></td>
            </tr>
            <tr>
                <td><strong>Number of read pages: </strong></td>
                <td></td>
            </tr>
            <tr>
                <td><strong>Top five authors: </strong></td>
                <td></td>
            </tr>
        </table>
    </div>
    <div class="table-responsive">
        <table>
            <tr>
                <th></th>
            </tr>
            <tr>
                <button style="margin: 10px"><a href="/user/book/add">Add new book to my diary</a></button>
            </tr>
            <tr>
                <td>
                    <button style="margin: 10px"><a href="/user/book/list">List of read books</a></button>
                </td>
            </tr>
            <tr>
                <td>
                    <button style="margin: 10px">My books on a Timeline</button>
                </td>
            </tr>
            <tr>
                <td>
                    <button style="margin: 10px">My books on the World map</button>
                </td>
            </tr>

        </table>

    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="${contextPath}/resources/js/bootstrap.min.js"></script>
</body>
</html>