package com.sda.cluj4.finalproject.validator;

import com.sda.cluj4.finalproject.model.Author;
import com.sda.cluj4.finalproject.service.AuthorService;
import com.sda.cluj4.finalproject.utils.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import static com.sda.cluj4.finalproject.security.validator.UserValidator.specialCharacters;

@Component
public class AuthorValidator implements Validator {
    public AuthorService authorService;

    @Autowired
    public AuthorValidator(AuthorService authorService) {
        this.authorService = authorService;
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return Author.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        Author author = (Author) o;

        String firstNameNoDiacritics = Utils.removeDiacriticalMarks(author.getFirstName());
        String lastNameNoDiacritics = Utils.removeDiacriticalMarks(author.getLastName());

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "firstName", "NotEmpty");

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "lastName", "NotEmpty");

        if (authorService.findAuthorByFirstNameAndLastName(author.getFirstName(), author.getLastName()) != null) {
            errors.rejectValue("firstName", "Duplicate.authorForm.fullName");
            errors.rejectValue("lastName", "Duplicate.authorForm.fullName");
        }

        if (authorService.findAuthorByFirstNameAndLastName(firstNameNoDiacritics, lastNameNoDiacritics) != null) {
            errors.rejectValue("firstName", "Duplicate.authorForm.fullName");
            errors.rejectValue("lastName", "Duplicate.authorForm.fullName");
        }

        for (String current : specialCharacters) {
            if (author.getFirstName().contains(current)) {
                errors.rejectValue("firstName", "Invalid.name");
            }
        }

        for (String current : specialCharacters) {
            if (author.getLastName().contains(current)) {
                errors.rejectValue("lastName", "Invalid.name");
            }
        }

        if (author.getFirstName().length() < 2 || author.getFirstName().length() > 32) {
            errors.rejectValue("firstName", "Size.name");
        }

        if (author.getLastName().length() < 2 || author.getLastName().length() > 32) {
            errors.rejectValue("lastName", "Size.name");
        }

        int counterBY = 0;
        int birthYear = author.getBirthYear();
        while (birthYear > 0) {
            birthYear = birthYear / 10;
            counterBY = counterBY + 1;
        }
        if (counterBY < 0 || counterBY > 4) {
            errors.rejectValue("birthYear", "Size.year");
        }

        int counterDY = 0;
        int deathYear = author.getDeathYear();
        while (deathYear > 0) {
            deathYear = deathYear / 10;
            counterDY = counterDY + 1;
        }
        if (counterDY < 0 || counterDY > 4) {
            errors.rejectValue("deathYear", "Size.year");
        }
    }
}
