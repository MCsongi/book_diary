package com.sda.cluj4.finalproject.validator;

import com.sda.cluj4.finalproject.model.Book;
import com.sda.cluj4.finalproject.service.BookService;
import com.sda.cluj4.finalproject.utils.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import static com.sda.cluj4.finalproject.security.validator.UserValidator.specialCharacters;

@Component
public class BookValidator implements Validator {
    public BookService bookService;

    @Autowired
    public BookValidator(BookService bookService) {
        this.bookService = bookService;
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return Book.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        Book book = (Book) o;

        String titleNoDiacritics = Utils.removeDiacriticalMarks(book.getTitle());
        String originalTitleNoDiacritics = Utils.removeDiacriticalMarks(book.getOriginalTitle());

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "title", "NotEmpty");

        if (bookService.findBookByTitle(book.getTitle()) != null ||
                bookService.findBookByOriginalTitle(book.getTitle()) != null) {
            errors.rejectValue("title", "Duplicate.bookForm.title");
        }

        if (bookService.findBookByTitle(titleNoDiacritics) != null ||
                bookService.findBookByOriginalTitle(titleNoDiacritics) != null) {
            errors.rejectValue("title", "Duplicate.bookForm.title");
        }

        if (bookService.findBookByTitle(book.getOriginalTitle()) != null ||
                bookService.findBookByOriginalTitle(book.getOriginalTitle()) != null) {
            errors.rejectValue("originalTitle", "Duplicate.bookForm.title");
        }

        if (bookService.findBookByTitle(originalTitleNoDiacritics) != null ||
                bookService.findBookByOriginalTitle(originalTitleNoDiacritics) != null) {
            errors.rejectValue("originalTitle", "Duplicate.bookForm.title");
        }

        if (book.getTitle().length() < 2 || book.getTitle().length() > 32) {
            errors.rejectValue("title", "Size.name");
        }

        if (book.getOriginalTitle().length() < 2 || book.getOriginalTitle().length() > 32) {
            errors.rejectValue("originalTitle", "Size.name");
        }

    }
}
