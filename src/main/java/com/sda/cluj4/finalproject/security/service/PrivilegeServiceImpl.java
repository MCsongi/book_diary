package com.sda.cluj4.finalproject.security.service;

import com.sda.cluj4.finalproject.security.dao.PrivilegeDao;
import com.sda.cluj4.finalproject.security.model.Privilege;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PrivilegeServiceImpl implements PrivilegeService {

    private PrivilegeDao privilegeDao;

    @Autowired
    public PrivilegeServiceImpl(PrivilegeDao privilegeDao) {
        this.privilegeDao = privilegeDao;
    }

    @Override
    public void save(Privilege privilege) {
        privilegeDao.save(privilege);
    }

    @Override
    public List<Privilege> getAllPrivileges() {
        return privilegeDao.findAll();
    }

    @Override
    public Privilege findByPrivilegeName(String privilege_name) {
        return privilegeDao.findByName(privilege_name);
    }

    @Override
    public void deletePrivilegeById(Long privilege_id) {
        privilegeDao.deleteById(privilege_id);
    }

    @Override
    public void deleteAllPrivileges() {
        privilegeDao.deleteAll();
    }
}
