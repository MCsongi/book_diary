package com.sda.cluj4.finalproject.security.validator;

import com.sda.cluj4.finalproject.security.model.User;
import com.sda.cluj4.finalproject.security.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.List;

@Component
public class UserValidator implements Validator {
    private UserService userService;

    public static List<String> specialCharacters = Arrays.asList
            ("!", "@", "#", "$", "%", "^", "&", "*", "(", ")", ",", ".", ";", ":", "'", ",", "_", "+", "=", "/", "<", ">", "?",
                    "\\", "\"", "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "|", "`", "[", "]", "{", "}");

    @Autowired
    public UserValidator(UserService userService) {
        this.userService = userService;
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return User.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        User user = (User) o;

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "username", "NotEmpty");

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "password", "NotEmpty");

        if (user.getUsername().length() < 6 || user.getUsername().length() > 32) {
            errors.rejectValue("username", "Size.userForm.username");
        }
        if (userService.findByUsername(user.getUsername()) != null) {
            errors.rejectValue("username", "Duplicate.userForm.username");
        }

        if (user.getPassword().length() < 8 || user.getPassword().length() > 32) {
            errors.rejectValue("password", "Size.userForm.password");
        }

        if (!user.getPasswordConfirm().equals(user.getPassword())) {
            errors.rejectValue("passwordConfirm", "Diff.userForm.passwordConfirm");
        }

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "firstName", "NotEmpty");

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "lastName", "NotEmpty");

        for (String current : specialCharacters) {
            if (user.getFirstName().contains(current)) {
                errors.rejectValue("firstName", "Invalid.name");
            }
        }

        for (String current : specialCharacters) {
            if (user.getLastName().contains(current)) {
                errors.rejectValue("lastName", "Invalid.name");
            }
        }

        if (user.getFirstName().length() < 2 || user.getFirstName().length() > 32) {
            errors.rejectValue("firstName", "Size.name");
        }

        if (user.getLastName().length() < 2 || user.getLastName().length() > 32) {
            errors.rejectValue("lastName", "Size.name");
        }

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "email", "NotEmpty");

        if (!user.getEmail().contains("@")) {
            errors.rejectValue("email", "Invalid.userForm.email");
        }

        if (user.getEmail().length() < 2 || user.getEmail().length() > 32) {
            errors.rejectValue("email", "Size.name");
        }

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "birthDate", "NotEmpty");
    }
}
