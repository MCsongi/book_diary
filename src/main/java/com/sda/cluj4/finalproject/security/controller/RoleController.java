package com.sda.cluj4.finalproject.security.controller;

import com.sda.cluj4.finalproject.security.model.Privilege;
import com.sda.cluj4.finalproject.security.model.Role;
import com.sda.cluj4.finalproject.security.service.PrivilegeService;
import com.sda.cluj4.finalproject.security.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.List;

@Controller
public class RoleController {
    private RoleService roleService;
    private PrivilegeService privilegeService;

    @Autowired
    public RoleController(RoleService roleService, PrivilegeService privilegeService) {
        this.roleService = roleService;
        this.privilegeService = privilegeService;
    }

    @RequestMapping(value = "/admin/role_privileges/list")
    public String rolesPrivilegesList(Model model) {
        List<Role> roles = roleService.getAllRoles();
        model.addAttribute("roleList", roles);
        List<Privilege> privileges = privilegeService.getAllPrivileges();
        model.addAttribute("privilegeList", privileges);
        return "rolesPrivilegesListPage";
    }

    @GetMapping(value = "/admin/role/add")
    public String addRolePage(Model model) {
        model.addAttribute("roleForm", new Role());
        return "createRolePage";
    }

    @PostMapping(value = "/admin/role/add")
    public String addRole(@ModelAttribute("roleForm") Role roleForm) {
        roleService.save(roleForm);
        return "redirect:/admin/role_privileges/list";
    }

    @RequestMapping(value = "/admin/role/delete/{id}", method = RequestMethod.POST)
    public String deleteRoleById(@RequestParam("role_id") Long role_id) {
        roleService.deleteRoleById(role_id);
        return "redirect:/admin/role_privileges/list";
    }

    @DeleteMapping(value = "/admin/role/list/delete")
    public String deleteAllRoles() {
        roleService.deleteRoles();
        return "redirect:/admin/role_privileges/list";
    }

    @GetMapping(value = "/admin/role/privileges/{id}")
    public String editRolesPage(@RequestParam("role_id") Long role_id, Model model) {
        Role role = roleService.getRoleById(role_id);
        model.addAttribute("roleForm", role);
        Collection<Privilege> privileges = privilegeService.getAllPrivileges();
        model.addAttribute("privilegeList", privileges);
        return "setRolePrivilegePage";
    }

    @PostMapping(value = "/admin/role/privileges/{id}")
    public String saveNewPrivileges(@ModelAttribute("roleForm") Role roleForm,
                                    @RequestParam(name="privileges", required=false) Collection<Privilege> privileges) {
        roleForm.setPrivileges(privileges);
        roleService.saveChangePrivileges(roleForm);
        return "redirect:/admin/role_privileges/list";
    }
}
