package com.sda.cluj4.finalproject.security.dao;

import com.sda.cluj4.finalproject.security.model.Role;
import com.sda.cluj4.finalproject.security.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserDao extends JpaRepository<User, Long> {
    User findByUsername(String username);
}
