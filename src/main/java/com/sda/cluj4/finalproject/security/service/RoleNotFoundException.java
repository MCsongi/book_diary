package com.sda.cluj4.finalproject.security.service;

public class RoleNotFoundException extends RuntimeException{
    public RoleNotFoundException(Long id){
        super("Could not find the Role with this id " + id);
    }
}
