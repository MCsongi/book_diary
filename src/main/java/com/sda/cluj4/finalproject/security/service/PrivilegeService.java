package com.sda.cluj4.finalproject.security.service;

import com.sda.cluj4.finalproject.security.model.Privilege;

import java.util.List;

public interface PrivilegeService {

    void save(Privilege privilege);

    List<Privilege> getAllPrivileges();

    Privilege findByPrivilegeName(String privilege_name);

    void deletePrivilegeById(Long privilege_id);

    void deleteAllPrivileges();
}
