package com.sda.cluj4.finalproject.security.service;

public class UserNotFoundException extends RuntimeException{
    public UserNotFoundException(Long id){
        super("Could not find the User with this id " + id);
    }
}
