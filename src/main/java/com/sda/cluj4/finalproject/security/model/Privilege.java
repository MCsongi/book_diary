package com.sda.cluj4.finalproject.security.model;

import javax.persistence.*;
import java.util.Collection;
import java.util.Objects;

@Entity
public class Privilege {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "privilege_id")
    private Long privilege_id;
    @Column(name = "privilege_name")
    private String name;

    public Privilege() {
    }

    public Privilege(String privilegeName) {
        this.name = privilegeName;
    }

    @ManyToMany(mappedBy = "privileges")
    private Collection<Role> roles;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Privilege)) return false;
        Privilege privilege = (Privilege) o;
        return Objects.equals(getPrivilege_id(), privilege.getPrivilege_id()) &&
                Objects.equals(getName(), privilege.getName()) &&
                Objects.equals(getRoles(), privilege.getRoles());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getPrivilege_id(), getName(), getRoles());
    }

    @Override
    public String toString() {
        return name;
    }

    public Long getPrivilege_id() {
        return privilege_id;
    }

    public void setPrivilege_id(Long privilege_id) {
        this.privilege_id = privilege_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Collection<Role> getRoles() {
        return roles;
    }

    public void setRoles(Collection<Role> roles) {
        this.roles = roles;
    }
}
