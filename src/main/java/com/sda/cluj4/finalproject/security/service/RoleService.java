package com.sda.cluj4.finalproject.security.service;

import com.sda.cluj4.finalproject.security.model.Role;

import java.util.List;

public interface RoleService {

    void save(Role role);

    List<Role> getAllRoles();

    void deleteRoleById(Long role_id);

    void deleteRoles();

    Role getRoleById(Long role_id);

    void saveChangePrivileges(Role role);

    Role findByRoleName(String role_name);
}
