package com.sda.cluj4.finalproject.security.service;

import com.sda.cluj4.finalproject.security.model.Role;
import com.sda.cluj4.finalproject.security.model.User;

import java.util.List;

public interface UserService {
    void saveNewUser(User user);

    void saveAdmin(User user);

    User findByUsername(String username);

    List<User> getAllUsers();

    List<User> findUsersByRole(String roleName);

    void deleteById(Long user_id);

    void deleteUsers();

    User getUserById(Long user_id);

    void saveEditUser(User user);

    void saveChangeRoles(User user);

    void saveAddBooks(User user);

}
