package com.sda.cluj4.finalproject.security.service;

import com.sda.cluj4.finalproject.security.dao.RoleDao;
import com.sda.cluj4.finalproject.security.model.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RoleServiceImpl implements RoleService {

    private RoleDao roleDao;

    @Autowired
    public RoleServiceImpl(RoleDao roleDao) {
        this.roleDao = roleDao;
    }

    @Override
    public void save(Role role) {
        roleDao.save(role);
    }

    @Override
    public List<Role> getAllRoles() {
        return roleDao.findAll();
    }

    @Override
    public void deleteRoleById(Long role_id) {
        roleDao.deleteById(role_id);
    }

    @Override
    public void deleteRoles() {
        List<Role> roles = roleDao.findAll();
        for (Role current : roles) {
            if (!current.getName().contains("ROLE_ADMIN")) {
                roleDao.delete(current);
            }
        }
    }

    @Override
    public Role getRoleById(Long role_id) {
        return roleDao.findById(role_id).orElseThrow(() -> new RoleNotFoundException((role_id)));
    }

    @Override
    public void saveChangePrivileges(Role role) {
        Role fromDB = roleDao.findById(role.getRole_id()).get();
        role.setName(fromDB.getName());
        role.setUsers(fromDB.getUsers());
        roleDao.save(role);
    }

    @Override
    public Role findByRoleName(String role_name) {
        return roleDao.findByName(role_name);
    }
}
