package com.sda.cluj4.finalproject.security.service;

public class PrivilegeNotFoundException extends RuntimeException{
    public PrivilegeNotFoundException(Long id){
        super("Could not find the Privilege with this id " + id);
    }
}
