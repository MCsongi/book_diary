package com.sda.cluj4.finalproject.security.controller;

import com.sda.cluj4.finalproject.model.Book;
import com.sda.cluj4.finalproject.security.model.Role;
import com.sda.cluj4.finalproject.security.model.User;
import com.sda.cluj4.finalproject.security.service.RoleService;
import com.sda.cluj4.finalproject.security.service.SecurityService;
import com.sda.cluj4.finalproject.security.service.UserService;
import com.sda.cluj4.finalproject.security.validator.UserValidator;
import com.sda.cluj4.finalproject.service.AuthorDetailsService;
import com.sda.cluj4.finalproject.service.BookDetailsService;
import com.sda.cluj4.finalproject.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.List;

@Controller
public class UserController {
    private UserService userService;
    private SecurityService securityService;
    private UserValidator userValidator;
    private RoleService roleService;
    private BookService bookService;
    private BookDetailsService bookDetailsService;
    private AuthorDetailsService authorDetailsService;

    @Autowired
    public UserController(UserService userService, SecurityService securityService, UserValidator userValidator,
                          RoleService roleService, BookService bookService, BookDetailsService bookDetailsService,
                          AuthorDetailsService authorDetailsService) {
        this.userService = userService;
        this.securityService = securityService;
        this.userValidator = userValidator;
        this.roleService = roleService;
        this.bookService = bookService;
        this.bookDetailsService = bookDetailsService;
        this.authorDetailsService = authorDetailsService;
    }

    @GetMapping("/registration")
    public String registration(Model model) {
        model.addAttribute("userForm", new User());
        return "registrationPage";
    }

    @PostMapping("/registration")
    public String registration(@ModelAttribute("userForm") User userForm, BindingResult bindingResult) {
        userValidator.validate(userForm, bindingResult);
        if (bindingResult.hasErrors()) {
            return "registrationPage";
        }
        userService.saveNewUser(userForm);
        securityService.autoLogin(userForm.getUsername(), userForm.getPasswordConfirm());
        return "redirect:/welcome";
    }

    @GetMapping("/login")
    public String login(Model model, String error, String logout) {
        if (error != null)
            model.addAttribute("error", "Your username and password is required or is invalid.");
        if (logout != null)
            model.addAttribute("message", "You have been logged out successfully.");
        return "loginPage";
    }

    @GetMapping({"/welcome"})
    public String welcome(Model model) {
        model.addAttribute("userForm", new User());
        return "welcomePage";
    }

    @GetMapping({"/", "/admin"})
    public String admin(Model model) {
        String username = securityService.findLoggedInUsername();
        User loggedUser = userService.findByUsername(username);
        if (loggedUser.getRoles().toString().contains("ROLE_ADMIN")) {
            return "adminPage";
        }
        return "redirect:/welcome";
    }

    @RequestMapping(value = "/admin/user/list")
    public String userList(Model model) {
        String username = securityService.findLoggedInUsername();
        User loggedUser = userService.findByUsername(username);
        if (loggedUser.getRoles().toString().contains("ROLE_ADMIN")) {
            List<User> users = userService.getAllUsers();
            model.addAttribute("userList", users);
            return "userListPage";
        }
        return "redirect:/welcome";
    }

    @RequestMapping(value = "/admin/user/delete/{id}", method = RequestMethod.POST)
    public String deleteUserById(@RequestParam("user_id") Long user_id) {
        userService.deleteById(user_id);
        return "redirect:/admin/user/list";
    }

    @DeleteMapping(value = "/admin/user/list/delete")
    public String deleteAllUsers() {
        userService.deleteUsers();
        return "redirect:/admin/user/list";
    }

    @GetMapping(value = "/admin/user/edit/{id}")
    public String editUserPage(@RequestParam("user_id") Long user_id, Model model) {
        User user = userService.getUserById(user_id);
        model.addAttribute("userForm", user);
        return "editUserPage";
    }

    @PostMapping(value = "/admin/user/edit/{id}")
    public String saveEdits(@ModelAttribute("userForm") User userForm) {
        userService.saveEditUser(userForm);
        return "redirect:/admin/user/list";
    }

    @GetMapping(value = "/admin/user/roles/{id}")
    public String editRolesPage(@RequestParam("user_id") Long user_id, Model model) {
        User user = userService.getUserById(user_id);
        model.addAttribute("userForm", user);
        Collection<Role> roles = roleService.getAllRoles();
        model.addAttribute("roleList", roles);
        return "setUserRolePage";
    }

    @PostMapping(value = "/admin/user/roles/{id}")
    public String saveNewRoles(@ModelAttribute("userForm") User userForm,
                               @RequestParam(name = "roles", required = false) Collection<Role> roles) {
        userForm.setRoles(roles);
        userService.saveChangeRoles(userForm);
        return "redirect:/admin/user/list";
    }

    @GetMapping(value = "/user/book/add")
    public String addUserBookPage(Model model) {
        String username = securityService.findLoggedInUsername();
        User loggedUser = userService.findByUsername(username);
        model.addAttribute("userForm", loggedUser);
        List<Book> books = bookService.getAllBooks();
        model.addAttribute("bookList", books);
        return "addReadBooksPage";
    }

    @PostMapping(value = "/user/book/add")
    public String saveAddBooks(@ModelAttribute("userForm") User userForm,
                               @RequestParam(name = "user_id", required = false) Long user_id,
                               @RequestParam(name = "selectedBooks", required = false) List<Book> books) {
        User user = userService.getUserById(user_id);
        Collection<Book> completedList = user.getBooks();
        for (Book current : books) {
            if (!completedList.contains(current)) {
                completedList.add(current);
            }
        }
        user.setBooks(completedList);
        userService.saveAddBooks(user);
        return "redirect:/user/book/list";
    }

    @RequestMapping(value = "/user/book/list")
    public String bookList(Model model) {
        String username = securityService.findLoggedInUsername();
        User loggedUser = userService.findByUsername(username);
        Collection<Book> books = loggedUser.getBooks();
        model.addAttribute("bookList", books);
        return "userBookListPage";
    }
}
