package com.sda.cluj4.finalproject.security.service;

public interface SecurityService {

    String findLoggedInUsername();

    void autoLogin(String username, String password);
}
