package com.sda.cluj4.finalproject.security.dao;

import com.sda.cluj4.finalproject.security.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleDao extends JpaRepository<Role, Long> {
    Role findByName(String roleName);
}
