package com.sda.cluj4.finalproject.security.dao;

import com.sda.cluj4.finalproject.security.model.Privilege;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PrivilegeDao extends JpaRepository<Privilege, Long> {
    Privilege findByName(String privilegeName);
}
