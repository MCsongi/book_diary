package com.sda.cluj4.finalproject.security.service;

import com.sda.cluj4.finalproject.security.model.User;
import com.sda.cluj4.finalproject.security.dao.RoleDao;
import com.sda.cluj4.finalproject.security.dao.UserDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {
    private UserDao userDao;
    private RoleDao roleDao;
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    public UserServiceImpl(UserDao userDao, RoleDao roleDao, BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.userDao = userDao;
        this.roleDao = roleDao;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    @Override
    public void saveNewUser(User user) {
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        user.setRoles(Arrays.asList(roleDao.findByName("ROLE_USER")));
        user.setEnabled(true);
        user.setTokenExpired(false);
        userDao.save(user);
    }

    @Override
    public void saveEditUser(User user) {
        User fromDB = userDao.findById(user.getUser_id()).get();
        user.setRoles(fromDB.getRoles());
        user.setAuthorDetails(fromDB.getAuthorDetails());
        user.setBookDetails(fromDB.getBookDetails());
        user.setBooks(fromDB.getBooks());
        userDao.save(user);
    }

    @Override
    public void saveChangeRoles(User user) {
        User fromDB = userDao.findById(user.getUser_id()).get();
        user.setFirstName(fromDB.getFirstName());
        user.setLastName(fromDB.getLastName());
        user.setUsername(fromDB.getUsername());
        user.setPassword(fromDB.getPassword());
        user.setBirthDate(fromDB.getBirthDate());
        user.setEmail(fromDB.getEmail());
        user.setEnabled(fromDB.isEnabled());
        user.setTokenExpired(fromDB.isTokenExpired());
        user.setAuthorDetails(fromDB.getAuthorDetails());
        user.setBookDetails(fromDB.getBookDetails());
        user.setBooks(fromDB.getBooks());
        userDao.save(user);
    }

    @Override
    public void saveAdmin(User user) {
        user.setRoles(Arrays.asList(roleDao.findByName("ROLE_ADMIN")));
        userDao.save(user);
    }

    @Override
    public User findByUsername(String username) {
        return userDao.findByUsername(username);
    }

    @Override
    public List<User> getAllUsers() {
        return userDao.findAll();
    }

    @Override
    public List<User> findUsersByRole(String roleName) {
        List<User> users = userDao.findAll();
        List<User> selected = null;
        for (User current : users) {
            if (current.getRoles().contains(roleName)) {
                selected.add(current);
            }
        }
        return selected;
    }

    @Override
    public void deleteById(Long user_id) {
        userDao.deleteById(user_id);
    }

    @Override
    public void deleteUsers() {
        List<User> users = userDao.findAll();
        for (User current : users) {
            if (!current.getRoles().contains("ROLE_ADMIN")) {
                userDao.delete(current);
            }
        }
    }

    @Override
    public User getUserById(Long user_id) {
        return userDao.findById(user_id).orElseThrow(() -> new UserNotFoundException((user_id)));
    }

    @Override
    public void saveAddBooks(User user) {
        User fromDB = userDao.findById(user.getUser_id()).get();
        user.setFirstName(fromDB.getFirstName());
        user.setLastName(fromDB.getLastName());
        user.setUsername(fromDB.getUsername());
        user.setPassword(fromDB.getPassword());
        user.setBirthDate(fromDB.getBirthDate());
        user.setEmail(fromDB.getEmail());
        user.setEnabled(fromDB.isEnabled());
        user.setTokenExpired(fromDB.isTokenExpired());
        user.setAuthorDetails(fromDB.getAuthorDetails());
        user.setBookDetails(fromDB.getBookDetails());
        user.setRoles(fromDB.getRoles());
        userDao.save(user);
    }
}