package com.sda.cluj4.finalproject.security.model;

import com.sda.cluj4.finalproject.security.service.PrivilegeService;
import com.sda.cluj4.finalproject.security.service.RoleService;
import com.sda.cluj4.finalproject.security.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.List;

@Component
public class InitialDataLoader implements
        ApplicationListener<ContextRefreshedEvent> {

    boolean alreadySetup = false;

    @Autowired
    private UserService userService;

    @Autowired
    private RoleService roleService;

    @Autowired
    private PrivilegeService privilegeService;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Override
    @Transactional
    public void onApplicationEvent(ContextRefreshedEvent event) {

        if (alreadySetup)
            return;

        Privilege readPrivilege = new Privilege("READ_PRIVILEGE");
        if (privilegeService.findByPrivilegeName(readPrivilege.getName()) == null) {
            privilegeService.save(readPrivilege);
        }

        Privilege writePrivilege = new Privilege("WRITE_PRIVILEGE");
        if (privilegeService.findByPrivilegeName(writePrivilege.getName()) == null) {
            privilegeService.save(writePrivilege);
        }

        Privilege deletePrivilege = new Privilege("DELETE_PRIVILEGE");
        if (privilegeService.findByPrivilegeName(deletePrivilege.getName()) == null) {
            privilegeService.save(deletePrivilege);
        }

        Privilege removePrivilege = new Privilege("REMOVE_PRIVILEGE");
        if (privilegeService.findByPrivilegeName(removePrivilege.getName()) == null) {
            privilegeService.save(removePrivilege);
        }

        List<Privilege> adminPrivileges = Arrays.asList(
                readPrivilege, writePrivilege, deletePrivilege, removePrivilege);
        Role adminRole = new Role("ROLE_ADMIN");
        adminRole.setPrivileges(adminPrivileges);
        if (roleService.findByRoleName(adminRole.getName()) == null) {
            roleService.save(adminRole);
        }

        List<Privilege> userPrivileges = Arrays.asList(
                readPrivilege, writePrivilege, removePrivilege);
        Role userRole = new Role("ROLE_USER");
        userRole.setPrivileges(userPrivileges);
        if (roleService.findByRoleName(userRole.getName()) == null) {
            roleService.save(userRole);
        }

        User user = new User();
        user.setFirstName("Csongor-Arpad");
        user.setLastName("Milik");
        user.setUsername("MCsongi");
        user.setPassword(bCryptPasswordEncoder.encode("admin"));
        user.setEmail("milik.csongor@gmail.com");
        user.setBirthDate(LocalDate.parse("1980-05-18", DateTimeFormatter.ofPattern("yyyy-MM-dd")));
        user.setEnabled(true);
        user.setTokenExpired(false);
        if (userService.findByUsername(user.getUsername()) == null) {
            userService.saveAdmin(user);
        }

        alreadySetup = true;
    }
}