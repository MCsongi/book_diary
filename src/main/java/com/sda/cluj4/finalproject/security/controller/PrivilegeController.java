package com.sda.cluj4.finalproject.security.controller;

import com.sda.cluj4.finalproject.security.model.Privilege;
import com.sda.cluj4.finalproject.security.service.PrivilegeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
public class PrivilegeController {
    private PrivilegeService privilegeService;

    @Autowired
    public PrivilegeController(PrivilegeService privilegeService) {
        this.privilegeService = privilegeService;
    }

    @GetMapping(value = "/admin/privilege/add")
    public String addPrivilegePage(Model model) {
        model.addAttribute("privilegeForm", new Privilege());
        return "createPrivilegePage";
    }

    @PostMapping(value = "/admin/privilege/add")
    public String addRole(@ModelAttribute("privilegeForm") Privilege privilegeForm) {
        privilegeService.save(privilegeForm);
        return "redirect:/admin/role_privileges/list";
    }

    @RequestMapping(value = "/admin/privilege/delete/{id}", method = RequestMethod.POST)
    public String deletePrivilegeById(@RequestParam("privilege_id") Long privilege_id) {
        privilegeService.deletePrivilegeById(privilege_id);
        return "redirect:/admin/role_privileges/list";
    }

    @DeleteMapping(value = "/admin/privilege/list/delete")
    public String deleteAllPrivileges() {
        privilegeService.deleteAllPrivileges();
        return "redirect:/admin/role_privileges/list";
    }
}
