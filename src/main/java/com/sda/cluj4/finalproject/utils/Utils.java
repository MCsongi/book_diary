package com.sda.cluj4.finalproject.utils;

import java.text.Normalizer;

public class Utils {
    public static String removeDiacriticalMarks(String string) {
        return Normalizer.normalize(string, Normalizer.Form.NFD)
                .replaceAll("\\p{InCombiningDiacriticalMarks}+", "");
    }
}
