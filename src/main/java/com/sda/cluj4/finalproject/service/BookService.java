package com.sda.cluj4.finalproject.service;

import com.sda.cluj4.finalproject.model.Author;
import com.sda.cluj4.finalproject.model.Book;

import java.util.Collection;
import java.util.List;

public interface BookService {

    void save(Book book);

    void deleteBookById(Long book_id);

    void deleteBooks();

    Book getBookById(Long book_id);

    List<Book> getAllBooks();

    void saveSetGenre(Book book);

    void setSelectedGenre(Book book, String genre);

    void saveEdits(Book book);

    Book findBookByTitle(String title);

    Book findBookByOriginalTitle(String originalTitle);

}
