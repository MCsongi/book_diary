package com.sda.cluj4.finalproject.service;

import com.sda.cluj4.finalproject.dao.AuthorDetailsDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AuthorDetailsServiceImpl implements AuthorDetailsService {
    public AuthorDetailsDao authorDetailsDao;

    @Autowired
    public AuthorDetailsServiceImpl(AuthorDetailsDao authorDetailsDao) {
        this.authorDetailsDao = authorDetailsDao;
    }
}
