package com.sda.cluj4.finalproject.service;

import com.sda.cluj4.finalproject.dao.BookDetailsDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BookDetailsServiceImpl implements BookDetailsService {
    public BookDetailsDao bookDetailsDao;

    @Autowired
    public BookDetailsServiceImpl(BookDetailsDao bookDetailsDao) {
        this.bookDetailsDao = bookDetailsDao;
    }
}
