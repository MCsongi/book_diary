package com.sda.cluj4.finalproject.service;

public class BookNotFoundException extends RuntimeException {
    public BookNotFoundException(Long id){
        super("Could not find the Book with this id " + id);
    }
}
