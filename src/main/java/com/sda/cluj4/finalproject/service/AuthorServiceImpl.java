package com.sda.cluj4.finalproject.service;

import com.sda.cluj4.finalproject.dao.AuthorDao;
import com.sda.cluj4.finalproject.model.Author;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class AuthorServiceImpl implements AuthorService {
    private AuthorDao authorDao;

    @Autowired
    public AuthorServiceImpl(AuthorDao authorDao) {
        this.authorDao = authorDao;
    }

    @Override
    public void save(Author author) {
        authorDao.save(author);
    }

    @Override
    public void deleteAuthorById(Long author_id) {
        authorDao.deleteById(author_id);
    }

    @Override
    public void deleteAuthors() {
        authorDao.deleteAll();
    }

    @Override
    public Author getAuthorById(Long author_id) {
        return authorDao.findById(author_id).orElseThrow(() -> new AuthorNotFoundException((author_id)));
    }

    @Override
    public List<Author> getAllAuthors() {
        return authorDao.findAll(Sort.by("firstName"));
    }

    @Override
    public Author findAuthorByFirstNameAndLastName(String firstName, String lastName) {
        return authorDao.findAuthorByFirstNameAndLastName(firstName, lastName);
    }

}
