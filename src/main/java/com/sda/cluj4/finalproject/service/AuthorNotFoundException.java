package com.sda.cluj4.finalproject.service;

public class AuthorNotFoundException extends RuntimeException {
    public AuthorNotFoundException(Long id){
        super("Could not find the Author with this id " + id);
    }
}
