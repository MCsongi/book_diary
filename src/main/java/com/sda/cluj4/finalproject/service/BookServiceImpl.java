package com.sda.cluj4.finalproject.service;

import com.sda.cluj4.finalproject.dao.BookDao;
import com.sda.cluj4.finalproject.dto.BookDto;
import com.sda.cluj4.finalproject.model.Author;
import com.sda.cluj4.finalproject.model.Book;
import com.sda.cluj4.finalproject.model.Genre;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;
import java.util.Set;

@Service
public class BookServiceImpl implements BookService {
    private BookDao bookDao;

    @Autowired
    public BookServiceImpl(BookDao bookDao) {
        this.bookDao = bookDao;
    }

    @Override
    public void save(Book book) {
        bookDao.save(book);
    }

    @Override
    public void deleteBookById(Long book_id) {
        bookDao.deleteById(book_id);
    }

    @Override
    public void deleteBooks() {
        bookDao.deleteAll();
    }

    @Override
    public Book getBookById(Long book_id) {
        return bookDao.findById(book_id).orElseThrow(() -> new BookNotFoundException((book_id)));
    }

    @Override
    public List<Book> getAllBooks() {
        return bookDao.findAll(Sort.by("authors").descending());
    }

    @Override
    public void saveSetGenre(Book book) {
        Book fromDB = bookDao.findById(book.getBook_id()).get();
        book.setTitle(fromDB.getTitle());
        book.setOriginalTitle(fromDB.getOriginalTitle());
        book.setReleaseYear(fromDB.getReleaseYear());
        book.setStartYear(fromDB.getStartYear());
        book.setEndYear(fromDB.getEndYear());
        book.setAuthors(fromDB.getAuthors());
        book.setUsers(fromDB.getUsers());
        book.setLocations(fromDB.getLocations());
        book.setBookDetails(fromDB.getBookDetails());
        bookDao.save(book);
    }

    @Override
    public void setSelectedGenre(Book book, String genre) {
        switch (genre) {
            case "ACTION, ADVENTURE":
                book.setGenre(Genre.ACTION_ADVENTURE.getName().toLowerCase());
                break;
            case "ANTHOLOGY":
                book.setGenre(Genre.ANTHOLOGY.getName().toLowerCase());
                break;
            case "CLASSIC":
                book.setGenre(Genre.CLASSIC.getName().toLowerCase());
                break;
            case "COMIC, GRAPHIC NOVEL":
                book.setGenre(Genre.COMIC_GRAPHIC_NOVEL.getName().toLowerCase());
                break;
            case "CRIME, DETECTIVE":
                book.setGenre(Genre.CRIME_DETECTIVE.getName().toLowerCase());
                break;
            case "DRAMA":
                book.setGenre(Genre.DRAMA.getName().toLowerCase());
                break;
            case "FABLE":
                book.setGenre(Genre.FABLE.getName().toLowerCase());
                break;
            case "FAIRY TALE":
                book.setGenre(Genre.FAIRY_TALE.getName().toLowerCase());
                break;
            case "FAN FICTION":
                book.setGenre(Genre.FAN_FICTION.getName().toLowerCase());
                break;
            case "FANTASY":
                book.setGenre(Genre.FANTASY.getName().toLowerCase());
                break;
            case "HISTORICAL FICTION":
                book.setGenre(Genre.HISTORICAL_FICTION.getName().toLowerCase());
                break;
            case "HORROR":
                book.setGenre(Genre.HORROR.getName().toLowerCase());
                break;
            case "HUMOR":
                book.setGenre(Genre.HUMOR.getName().toLowerCase());
            case "LEGEND":
                book.setGenre(Genre.LEGEND.getName().toLowerCase());
                break;
            case "MAGICAL REALISM":
                book.setGenre(Genre.MAGICAL_REALISM.getName().toLowerCase());
                break;
            case "MYSTERY":
                book.setGenre(Genre.MYSTERY.getName().toLowerCase());
                break;
            case "MYTHOLOGY":
                book.setGenre(Genre.MYTHOLOGY.getName().toLowerCase());
                break;
            case "REALISTIC FICTION":
                book.setGenre(Genre.REALISTIC_FICTION.getName().toLowerCase());
                break;
            case "ROMANCE":
                book.setGenre(Genre.ROMANCE.getName().toLowerCase());
                break;
            case "SATIRE":
                book.setGenre(Genre.SATIRE.getName().toLowerCase());
                break;
            case "SCIENCE FICTION":
                book.setGenre(Genre.SCIENCE_FICTION.getName().toLowerCase());
                break;
            case "SHORT STORY":
                book.setGenre(Genre.SHORT_STORY.getName().toLowerCase());
                break;
            case "SUSPENSE, THRILLER":
                book.setGenre(Genre.SUSPENSE_THRILLER.getName().toLowerCase());
                break;
            case "BIOGRAPHY, AUTOBIOGRAPHY":
                book.setGenre(Genre.BIOGRAPHY_AUTOBIOGRAPHY.getName().toLowerCase());
                break;
            case "ESSAY":
                book.setGenre(Genre.ESSAY.getName().toLowerCase());
                break;
            case "MEMOIR":
                book.setGenre(Genre.MEMOIR.getName().toLowerCase());
                break;
            case "NARRATIVE NONFICTION":
                book.setGenre(Genre.NARRATIVE_NONFICTION.getName().toLowerCase());
                break;
            case "PERIODICALS":
                book.setGenre(Genre.PERIODICALS.getName().toLowerCase());
                break;
            case "REFERENCE BOOKS":
                book.setGenre(Genre.REFERENCE_BOOKS.getName().toLowerCase());
                break;
            case "SELF HELP BOOK":
                book.setGenre(Genre.SELF_HELP_BOOK.getName().toLowerCase());
                break;
            case "SPEECH":
                book.setGenre(Genre.SPEECH.getName().toLowerCase());
                break;
            case "TEXTBOOK":
                book.setGenre(Genre.TEXTBOOK.getName().toLowerCase());
                break;
            case "POETRY":
                book.setGenre(Genre.POETRY.getName().toLowerCase());
                break;
            case "UNKNOWN":
                book.setGenre(Genre.UNKNOWN.getName().toLowerCase());
                break;
        }
    }

    @Override
    public void saveEdits(Book book) {
        Book fromDB = bookDao.findById(book.getBook_id()).get();
        book.setAuthors(fromDB.getAuthors());
        book.setGenre(fromDB.getGenre());
        book.setUsers(fromDB.getUsers());
        book.setLocations(fromDB.getLocations());
        book.setBookDetails(fromDB.getBookDetails());
        bookDao.save(book);
    }

    @Override
    public Book findBookByTitle(String title) {
        return bookDao.findBookByTitle(title);
    }

    @Override
    public Book findBookByOriginalTitle(String originalTitle) {
        return bookDao.findBookByOriginalTitle(originalTitle);
    }
}
