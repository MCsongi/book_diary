package com.sda.cluj4.finalproject.service;

import com.sda.cluj4.finalproject.model.Author;

import java.util.Collection;
import java.util.List;
import java.util.Set;

public interface AuthorService {

    void save(Author author);

    void deleteAuthorById(Long author_id);

    void deleteAuthors();

    Author getAuthorById(Long author_id);

    List<Author> getAllAuthors();

    Author findAuthorByFirstNameAndLastName(String firstName, String lastName);
}
