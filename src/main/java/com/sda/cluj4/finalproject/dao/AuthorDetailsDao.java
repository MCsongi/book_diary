package com.sda.cluj4.finalproject.dao;

import com.sda.cluj4.finalproject.model.AuthorDetails;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AuthorDetailsDao extends JpaRepository<AuthorDetails, Long> {
}
