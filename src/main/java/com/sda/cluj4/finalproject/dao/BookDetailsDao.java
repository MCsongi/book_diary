package com.sda.cluj4.finalproject.dao;

import com.sda.cluj4.finalproject.model.BookDetails;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BookDetailsDao extends JpaRepository<BookDetails, Long> {
}
