package com.sda.cluj4.finalproject.dao;

import com.sda.cluj4.finalproject.model.Book;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BookDao extends JpaRepository<Book, Long> {

    Book findBookByTitle(String title);

    Book findBookByOriginalTitle(String originalTitle);
}
