package com.sda.cluj4.finalproject.model;

public enum Continent {
    EUROPE,
    AFRICA,
    ASIA,
    AUSTRALIA_OCEANIA,
    NORTH_AMERICA,
    SOUTH_AMERICA,
    ANTARCTICA;

    @Override
    public String toString() {
        return name().charAt(0) + name().substring(1).toLowerCase();
    }
}
