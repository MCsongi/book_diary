package com.sda.cluj4.finalproject.model;

import com.sda.cluj4.finalproject.security.model.User;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.Year;
import java.util.Collection;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "book")
public class Book {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "book_id")
    private Long book_id;
    @Column(name = "title")
    private String title;
    @Column(name = "original_title")
    private String originalTitle;
    @Column(name = "release_year")
    private int releaseYear;
    @Column(name = "genre")
    private String genre;
    @Column(name = "start_year")
    private int startYear;
    @Column(name = "end_year")
    private int endYear;

    @Transient
    private String plotPeriod;

    @PostLoad
    public void buildPlotPeriod() {
        if (startYear == endYear) {
            this.plotPeriod = startYear + "";
        } else if (startYear == 0) {
            this.plotPeriod = endYear + "";
        } else if (endYear == 0) {
            this.plotPeriod = startYear + "";
        } else
            this.plotPeriod = startYear + "-" + endYear;
    }

    @ManyToMany
    @JoinTable(name = "author_book",
            joinColumns = {@JoinColumn(name = "book_id")},
            inverseJoinColumns = {@JoinColumn(name = "author_id")})
    Collection<Author> authors;

    @ManyToMany(mappedBy = "books")
    Collection<User> users;

    @ManyToMany
    @JoinTable(name = "book_location",
            joinColumns = {@JoinColumn(name = "book_id")},
            inverseJoinColumns = {@JoinColumn(name = "location_id")})
    Collection<Location> locations;

    @OneToMany(mappedBy = "book", fetch = FetchType.EAGER)
    Collection<BookDetails> bookDetails;

    public Book() {
    }

    public Long getBook_id() {
        return book_id;
    }

    public void setBook_id(Long book_id) {
        this.book_id = book_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getOriginalTitle() {
        return originalTitle;
    }

    public void setOriginalTitle(String originalTitle) {
        this.originalTitle = originalTitle;
    }

    public int getReleaseYear() {
        return releaseYear;
    }

    public void setReleaseYear(int releaseYear) {
        this.releaseYear = releaseYear;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public int getStartYear() {
        return startYear;
    }

    public void setStartYear(int startYear) {
        this.startYear = startYear;
    }

    public int getEndYear() {
        return endYear;
    }

    public void setEndYear(int endYear) {
        this.endYear = endYear;
    }

    public String getPlotPeriod() {
        return plotPeriod;
    }

    public void setPlotPeriod(String plotPeriod) {
        this.plotPeriod = plotPeriod;
    }

    public Collection<Author> getAuthors() {
        return authors;
    }

    public void setAuthors(Collection<Author> authors) {
        this.authors = authors;
    }

    public Collection<BookDetails> getBookDetails() {
        return bookDetails;
    }

    public void setBookDetails(Collection<BookDetails> bookDetails) {
        this.bookDetails = bookDetails;
    }

    public Collection<Location> getLocations() {
        return locations;
    }

    public void setLocations(Collection<Location> locations) {
        this.locations = locations;
    }

    public Collection<User> getUsers() {
        return users;
    }

    public void setUsers(Collection<User> users) {
        this.users = users;
    }
}
