package com.sda.cluj4.finalproject.model;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.Year;
import java.util.Collection;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "author")
public class Author {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "author_id")
    private Long author_id;
    @Column(name = "first_name")
    private String firstName;
    @Column(name = "last_name")
    private String lastName;
    @Column(name = "birth_year")
    private int birthYear;
    @Column(name = "death_year")
    private int deathYear;

    @Transient
    private String fullName;

    @PostLoad
    public void buildFirstName() {
        this.fullName = firstName + " " + lastName;
    }

    public Author() {
    }
    @ManyToMany(mappedBy = "authors")
    Collection<Book> books;

    @OneToMany(mappedBy = "author", fetch = FetchType.EAGER)
    Collection<AuthorDetails> authorDetails;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Author)) return false;
        Author author = (Author) o;
        return getBirthYear() == author.getBirthYear() &&
                getDeathYear() == author.getDeathYear() &&
                Objects.equals(getAuthor_id(), author.getAuthor_id()) &&
                Objects.equals(getFirstName(), author.getFirstName()) &&
                Objects.equals(getLastName(), author.getLastName()) &&
                Objects.equals(getFullName(), author.getFullName()) &&
                Objects.equals(getBooks(), author.getBooks()) &&
                Objects.equals(getAuthorDetails(), author.getAuthorDetails());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getAuthor_id(), getFirstName(), getLastName(), getBirthYear(), getDeathYear(), getFullName(), getBooks(), getAuthorDetails());
    }

    @Override
    public String toString() {
        return firstName + " " + lastName;
    }

    public Long getAuthor_id() {
        return author_id;
    }

    public void setAuthor_id(Long author_id) {
        this.author_id = author_id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getBirthYear() {
        return birthYear;
    }

    public void setBirthYear(int birthYear) {
        this.birthYear = birthYear;
    }

    public int getDeathYear() {
        return deathYear;
    }

    public void setDeathYear(int deathYear) {
        this.deathYear = deathYear;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public Collection<Book> getBooks() {
        return books;
    }

    public void setBooks(Collection<Book> books) {
        this.books = books;
    }

    public Collection<AuthorDetails> getAuthorDetails() {
        return authorDetails;
    }

    public void setAuthorDetails(Collection<AuthorDetails> authorDetails) {
        this.authorDetails = authorDetails;
    }

}
