package com.sda.cluj4.finalproject.model;

import javax.persistence.*;
import java.util.Collection;
import java.util.Objects;
import java.util.Set;
@Entity
@Table(name = "location")
public class Location {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "location_id")
    private Long location_id;
    @Enumerated(EnumType.STRING)
    @Column(name = "continent")
    private Continent continent;
    @Column(name = "country")
    private String country;
    @Column(name = "city")
    private String city;

    @ManyToMany(mappedBy = "locations")
    Collection<Book> books;

    public Location() {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Location)) return false;
        Location location = (Location) o;
        return Objects.equals(getLocation_id(), location.getLocation_id()) &&
                getContinent() == location.getContinent() &&
                Objects.equals(getCountry(), location.getCountry()) &&
                Objects.equals(getCity(), location.getCity()) &&
                Objects.equals(getBooks(), location.getBooks());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getLocation_id(), getContinent(), getCountry(), getCity(), getBooks());
    }

    @Override
    public String toString() {
        return "Location{" +
                "continent=" + continent +
                ", country='" + country + '\'' +
                ", city='" + city + '\'' +
                '}';
    }

    public Long getLocation_id() {
        return location_id;
    }

    public void setLocation_id(Long location_id) {
        this.location_id = location_id;
    }

    public Continent getContinent() {
        return continent;
    }

    public void setContinent(Continent continent) {
        this.continent = continent;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Collection<Book> getBooks() {
        return books;
    }

    public void setBooks(Collection<Book> books) {
        this.books = books;
    }
}
