package com.sda.cluj4.finalproject.model;

import com.sda.cluj4.finalproject.security.model.User;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "book_details")
public class BookDetails {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "book_details_id")
    private Long book_details_id;
    @Column(name = "plot")
    private String plot;
    @Column(name = "characters")
    private String characters;
    @Column(name = "number_of_pages")
    private int numberOfPages;

    @ManyToOne
    @JoinColumn(name = "book_id")
    private Book book;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id")
    private User user;

    public BookDetails() {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof BookDetails)) return false;
        BookDetails that = (BookDetails) o;
        return getNumberOfPages() == that.getNumberOfPages() &&
                Objects.equals(getBook_details_id(), that.getBook_details_id()) &&
                Objects.equals(getPlot(), that.getPlot()) &&
                Objects.equals(getCharacters(), that.getCharacters()) &&
                Objects.equals(getBook(), that.getBook()) &&
                Objects.equals(getUser(), that.getUser());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getBook_details_id(), getPlot(), getCharacters(), getNumberOfPages(), getBook(), getUser());
    }

    public Long getBook_details_id() {
        return book_details_id;
    }

    public void setBook_details_id(Long book_details_id) {
        this.book_details_id = book_details_id;
    }

    public String getPlot() {
        return plot;
    }

    public void setPlot(String plot) {
        this.plot = plot;
    }

    public String getCharacters() {
        return characters;
    }

    public void setCharacters(String characters) {
        this.characters = characters;
    }

    public int getNumberOfPages() {
        return numberOfPages;
    }

    public void setNumberOfPages(int numberOfPages) {
        this.numberOfPages = numberOfPages;
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
