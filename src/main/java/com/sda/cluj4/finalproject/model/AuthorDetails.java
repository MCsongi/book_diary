package com.sda.cluj4.finalproject.model;

import com.sda.cluj4.finalproject.security.model.User;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "author_details")
public class AuthorDetails {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "author_details_id")
    private Long author_details_id;
    @Column(name = "about")
    private String about;

    @ManyToOne
    @JoinColumn(name = "author_id")
    private Author author;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id")
    private User user;

    public AuthorDetails() {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AuthorDetails)) return false;
        AuthorDetails that = (AuthorDetails) o;
        return Objects.equals(getAuthor_details_id(), that.getAuthor_details_id()) &&
                Objects.equals(getAbout(), that.getAbout()) &&
                Objects.equals(getAuthor(), that.getAuthor()) &&
                Objects.equals(getUser(), that.getUser());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getAuthor_details_id(), getAbout(), getAuthor(), getUser());
    }

    public Long getAuthor_details_id() {
        return author_details_id;
    }

    public void setAuthor_details_id(Long author_details_id) {
        this.author_details_id = author_details_id;
    }

    public String getAbout() {
        return about;
    }

    public void setAbout(String about) {
        this.about = about;
    }

    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
