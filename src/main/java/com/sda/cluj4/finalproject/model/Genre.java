package com.sda.cluj4.finalproject.model;

public enum Genre {
    ACTION_ADVENTURE("ACTION, ADVENTURE"),
    ANTHOLOGY("ANTHOLOGY"),
    CLASSIC("CLASSIC"),
    COMIC_GRAPHIC_NOVEL("COMIC, GRAPHIC NOVEL"),
    CRIME_DETECTIVE("CRIME, DETECTIVE"),
    DRAMA("DRAMA"),
    FABLE("FABLE"),
    FAIRY_TALE("FAIRY TALE"),
    FAN_FICTION("FAN FICTION"),
    FANTASY("FANTASY"),
    HISTORICAL_FICTION("HISTORICAL FICTION"),
    HORROR("HORROR"),
    HUMOR("HUMOR"),
    LEGEND("LEGEND"),
    MAGICAL_REALISM("MAGICAL REALISM"),
    MYSTERY("MYSTERY"),
    MYTHOLOGY("MYTHOLOGY"),
    REALISTIC_FICTION("REALISTIC FICTION"),
    ROMANCE("ROMANCE"),
    SATIRE("SATIRE"),
    SCIENCE_FICTION("SCIENCE FICTION"),
    SHORT_STORY("SHORT STORY"),
    SUSPENSE_THRILLER("SUSPENSE, THRILLER"),
    BIOGRAPHY_AUTOBIOGRAPHY("BIOGRAPHY, AUTOBIOGRAPHY"),
    ESSAY("ESSAY"),
    MEMOIR("MEMOIR"),
    NARRATIVE_NONFICTION("NARRATIVE NONFICTION"),
    PERIODICALS("PERIODICALS"),
    REFERENCE_BOOKS("REFERENCE BOOKS"),
    SELF_HELP_BOOK("SELF HELP BOOK"),
    SPEECH("SPEECH"),
    TEXTBOOK("TEXTBOOK"),
    POETRY("POETRY"),
    UNKNOWN("UNKNOWN");

    private String name;

    Genre(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
