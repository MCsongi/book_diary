package com.sda.cluj4.finalproject.dto;

import com.sda.cluj4.finalproject.model.Author;
import com.sda.cluj4.finalproject.model.Book;
import javafx.print.Collation;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Collection;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class BookDto {
    private Book book;
    private Collection<Author> authors;
}
