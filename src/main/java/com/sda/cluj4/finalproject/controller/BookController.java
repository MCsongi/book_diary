package com.sda.cluj4.finalproject.controller;

import com.sda.cluj4.finalproject.model.Author;
import com.sda.cluj4.finalproject.model.Book;
import com.sda.cluj4.finalproject.model.Genre;
import com.sda.cluj4.finalproject.security.model.User;
import com.sda.cluj4.finalproject.security.service.SecurityService;
import com.sda.cluj4.finalproject.security.service.UserService;
import com.sda.cluj4.finalproject.service.AuthorService;
import com.sda.cluj4.finalproject.service.BookService;
import com.sda.cluj4.finalproject.validator.BookValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@Controller
public class BookController {
    private BookService bookService;
    private BookValidator bookValidator;
    private AuthorService authorService;
    private UserService userService;
    private SecurityService securityService;

    @Autowired
    public BookController(BookService bookService, BookValidator bookValidator,
                          AuthorService authorService, UserService userService,
                          SecurityService securityService) {
        this.bookService = bookService;
        this.bookValidator = bookValidator;
        this.authorService = authorService;
        this.userService = userService;
        this.securityService = securityService;
    }

    @RequestMapping(value = "/admin/book/list")
    public String bookList(Model model) {
        String username = securityService.findLoggedInUsername();
        User loggedUser = userService.findByUsername(username);
        if (loggedUser.getRoles().toString().contains("ROLE_ADMIN")) {
            List<Book> books = bookService.getAllBooks();
            model.addAttribute("bookList", books);
            return "bookListPage";
        }
        return "redirect:/welcome";
    }

    @GetMapping(value = "/admin/book/add")
    public String addBookPage(Model model) {
        model.addAttribute("bookForm", new Book());
        return "createBookPage";
    }

    @PostMapping(value = "/admin/book/add")
    public String addBook(@ModelAttribute("bookForm") Book bookForm,
                          BindingResult bindingResult) {
        bookValidator.validate(bookForm, bindingResult);
        if (bindingResult.hasErrors()) {
            return "createBookPage";
        }
        bookService.save(bookForm);
        return "redirect:/admin/book/list";
    }

    @GetMapping(value = "/user/book/addNew")
    public String userAddBookPage(Model model) {
        model.addAttribute("bookForm", new Book());
        return "userCreateBookPage";
    }

    @PostMapping(value = "/user/book/addNew")
    public String userAddBook(@ModelAttribute("bookForm") Book bookForm,
                              BindingResult bindingResult) {
        bookValidator.validate(bookForm, bindingResult);
        if (bindingResult.hasErrors()) {
            return "userCreateBookPage";
        }
        bookService.save(bookForm);
        return "redirect:/user/book/list";
    }

    @RequestMapping(value = "/admin/book/delete/{id}", method = RequestMethod.POST)
    public String deleteBookById(@RequestParam("book_id") Long book_id) {
        bookService.deleteBookById(book_id);
        return "redirect:/admin/book/list";
    }

    @DeleteMapping(value = "/admin/book/list/delete")
    public String deleteAllBooks() {
        bookService.deleteBooks();
        return "redirect:/admin/book/list";
    }

    @GetMapping(value = "/admin/book/edit/{id}")
    public String editBookPage(@RequestParam("book_id") Long book_id, Model model) {
        model.addAttribute("bookForm", bookService.getBookById(book_id));
        return "editBookPage";
    }

    @PostMapping("/admin/book/edit/{id}")
    public String saveEdits(@ModelAttribute("bookForm") Book bookForm) {
        bookService.saveEdits(bookForm);
        return "redirect:/book/list";
    }

    @GetMapping(value = "/admin/book/genre/{id}")
    public String editGenrePage(@RequestParam("book_id") Long book_id, Model model) {
        Book book = bookService.getBookById(book_id);
        model.addAttribute("bookForm", book);
        List<Genre> genres = new ArrayList<Genre>(Arrays.asList(Genre.values()));
        model.addAttribute("genreList", genres);
        return "setBookGenrePage";
    }

    @PostMapping(value = "/admin/book/genre/{id}")
    public String setGenre(@ModelAttribute("bookForm") Book bookForm,
                           @RequestParam(name = "genre", required = false) String genre) {
        if (genre == null) {
            return "redirect:/admin/book/list";
        }
        bookService.setSelectedGenre(bookForm, genre);
        bookService.saveSetGenre(bookForm);
        return "redirect:/admin/book/list";
    }

    @GetMapping(value = "/admin/book/author/{id}")
    public String editAuthorsPage(@RequestParam("book_id") Long book_id, Model model) {
        Book book = bookService.getBookById(book_id);
        model.addAttribute("bookForm", book);
        List<Author> authors = authorService.getAllAuthors();
        model.addAttribute("authorList", authors);
        return "setBookAuthorsPage";
    }

    @PostMapping(value = "/admin/book/author/{id}")
    public String setAuthors(@ModelAttribute("bookForm") Book bookForm,
                             @RequestParam(name = "book_id", required = false) Long book_id,
                             @RequestParam(name = "authors", required = false) List<Author> authors) {
        Book book = bookService.getBookById(book_id);
        book.setAuthors(authors);
        bookService.save(book);
        return "redirect:/admin/book/list";
    }

    @GetMapping(value = "/user/book/author/{id}")
    public String userEditAuthorsPage(@RequestParam("book_id") Long book_id, Model model) {
        Book book = bookService.getBookById(book_id);
        model.addAttribute("bookForm", book);
        List<Author> authors = authorService.getAllAuthors();
        model.addAttribute("authorList", authors);
        return "userSetBookAuthorsPage";
    }

    @PostMapping(value = "/user/book/author/{id}")
    public String userSetAuthors(@ModelAttribute("bookForm") Book bookForm,
                                 @RequestParam(name = "book_id", required = false) Long book_id,
                                 @RequestParam(name = "authors", required = false) List<Author> authors) {
        Book book = bookService.getBookById(book_id);
        book.setAuthors(authors);
        bookService.save(book);
        return "redirect:/user/book/list";
    }
}
