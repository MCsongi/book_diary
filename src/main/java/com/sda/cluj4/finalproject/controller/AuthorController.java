package com.sda.cluj4.finalproject.controller;

import com.sda.cluj4.finalproject.model.Author;
import com.sda.cluj4.finalproject.security.model.User;
import com.sda.cluj4.finalproject.security.service.SecurityService;
import com.sda.cluj4.finalproject.security.service.UserService;
import com.sda.cluj4.finalproject.service.AuthorService;
import com.sda.cluj4.finalproject.validator.AuthorValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
public class AuthorController {
    private AuthorService authorService;
    private AuthorValidator authorValidator;
    private UserService userService;
    private SecurityService securityService;

    @Autowired
    public AuthorController(AuthorService authorService, AuthorValidator authorValidator, UserService userService, SecurityService securityService) {
        this.authorService = authorService;
        this.authorValidator = authorValidator;
        this.userService = userService;
        this.securityService = securityService;
    }

    @RequestMapping(value = "/admin/author/list")
    public String authorList(Model model) {
        String username = securityService.findLoggedInUsername();
        User loggedUser = userService.findByUsername(username);
        if (loggedUser.getRoles().toString().contains("ROLE_ADMIN")) {
            List<Author> authors = authorService.getAllAuthors();
            model.addAttribute("authorList", authors);
            return "authorListPage";
        }
        return "redirect:/welcome";
    }

    @GetMapping(value = "/admin/author/add")
    public String addAuthorPage(Model model) {
        String username = securityService.findLoggedInUsername();
        User loggedUser = userService.findByUsername(username);
        if (loggedUser.getRoles().toString().contains("ROLE_ADMIN")) {
            model.addAttribute("authorForm", new Author());
            return "createAuthorPage";
        }
        return "redirect:/welcome";
    }

    @PostMapping(value = "/admin/author/add")
    public String addAuthor(@ModelAttribute("authorForm") Author authorForm, BindingResult bindingResult) {
        authorValidator.validate(authorForm, bindingResult);
        if (bindingResult.hasErrors()) {
            return "createAuthorPage";
        }
        authorService.save(authorForm);
        return "redirect:/admin/author/list";
    }

    @GetMapping(value = "/user/author/add")
    public String userAddAuthorPage(Model model) {
        model.addAttribute("authorForm", new Author());
        return "userCreateAuthorPage";
    }

    @PostMapping(value = "/user/author/add")
    public String userAddAuthor(@ModelAttribute("authorForm") Author authorForm, BindingResult bindingResult) {
        authorValidator.validate(authorForm, bindingResult);
        if (bindingResult.hasErrors()) {
            return "userCreateAuthorPage";
        }
        authorService.save(authorForm);
        return "redirect:/user/book/addNew";
    }

    @RequestMapping(value = "/admin/author/delete/{id}", method = RequestMethod.POST)
    public String deleteAuthorById(@RequestParam("author_id") Long author_id) {
        authorService.deleteAuthorById(author_id);
        return "redirect:/admin/author/list";
    }

    @DeleteMapping(value = "/admin/author/list/delete")
    public String deleteAllAuthors() {
        authorService.deleteAuthors();
        return "redirect:/admin/author/list";
    }

    @GetMapping(value = "/admin/author/edit/{id}")
    public String editAuthorPage(@RequestParam("author_id") Long author_id, Model model) {
        model.addAttribute("authorForm", authorService.getAuthorById(author_id));
        return "editAuthorPage";
    }

    @PostMapping("/admin/author/edit/{id}")
    public String saveEdits(@ModelAttribute("authorForm") Author authorForm) {
        authorService.save(authorForm);
        return "redirect:/admin/author/list";
    }
}